SO<sub>2</sub> Flux Calculator
==============

**Python implementation of the volcanic SO<sub>2</sub> flux calculator add-on of the VolcPlume platform**

**Developed primarily for exploitation of TROPOMI satellite imagery**

**Authors:** *Raphael Grandin, Marie Boichu, Théo Mathurin, Nicolas Pascal*


## Algorithm workflow

This package includes a command-line interface for the Flux Calculator algorithm described in the paper ["*Automatic estimation of daily volcanic sulfur dioxide gas flux from TROPOMI satellite observations: application to Etna and Piton de la Fournaise*"](https://doi.org/10.1029/2024JB029309).

[figure0]: doc/diagram_flux_calculation_V5_disk_method.png#center "Figure 0"
![Algorithm workflow][figure0]

## Citation

If you find this code useful, please cite:

```quote
Grandin, R., Boichu, M., Mathurin, T.,
& Pascal, N. (2024). Automatic estimation
of daily volcanic sulfur dioxide gas flux
from TROPOMI satellite observations:
application to Etna and Piton de la Fournaise.
Journal of Geophysical Research: Solid Earth,
129(6), e2024JB029309. doi: 10.1029/2024JB029309
```


LaTeX bibtex:

```quote
@article{grandin2024automatic,
  title={{Automatic estimation of daily volcanic sulfur dioxide gas flux from TROPOMI satellite observations: application to Etna and Piton de la Fournaise}},
  author={Grandin, Raphael and Boichu, Marie and Mathurin, Th{\'e}o and Pascal, Nicolas},
  journal={Journal of Geophysical Research: Solid Earth},
  volume={129},
  number={6},
  pages={e2024JB029309},
  year={2024},
  publisher={Wiley Online Library},
  doi={10.1029/2024JB029309},
}
```

This algorithm is also implemented in the "so2-flux-calculator" web app [(https://dataviz.icare.univ-lille.fr/so2-flux-calculator)](https://dataviz.icare.univ-lille.fr/so2-flux-calculator), within the VolcPlume portal (Boichu et al., 2022, [https://www.icare.univ-lille.fr/volcplume/](https://www.icare.univ-lille.fr/volcplume/)).

[figure0b]: doc/diagram_flux_calculation_V5_VSO.png#center "Figure 0"
![Integration within the VolcPlume portal][figure0b]

If you used the web app implementation from the VolcPlume portal (accessible via [https://dataviz.icare.univ-lille.fr/so2-flux-calculator](https://dataviz.icare.univ-lille.fr/so2-flux-calculator)), please add the following citation to your list of bibliographical references:

```quote
Boichu, M and Mathurin, T (2022),
"VOLCPLUME, an interactive web portal for the multiscale analysis
of volcanic plume physico-chemical properties.".
[Interactive Web based Ressource]
url: https://www.icare.univ-lille.fr/volcplume/
doi: 10.25326/362
```

LaTeX bibtex:

```quote
@misc{boichu2022volcplume,
  title={{VOLCPLUME, an interactive web portal for the multiscale analysis of volcanic plume physico-chemical properties. [Interactive Web based Ressource], AERIS}},
  author={Boichu, M and Mathurin, T},
  year={2022},
  note = {Website address: \url{https://www.icare.univ-lille.fr/volcplume/}},
  url={https://volcplume.aeris-data.fr/},
  doi={10.25326/362}
}
```



## Package content

```quote
so2-flux-calculator/ ........... Project directory
  |-- LICENSE .................. License under which project is distributed
  |-- README.md ................ Project README
  |-- doc/ ..................... Documentation files  
  |-- environment.yml .......... Conda environment
  |-- pyproject.toml ........... Package metadata
  |-- sample_inputs/ ........... Sample input datasets (data analyzed in the paper)  
  |  |-- Etna/ ................. Sample input data for Etna
  |  |-- Piton/ ................ Sample input data for Piton de la Fournaise
  |-- setup.py ................. Package setup file
  |-- src/ ..................... Source code directory
  |  |-- so2_flux_calculator/
  |  |-- __constants__.py ...... A few constants
  |  |-- __init__.py ........... Module file
  |  |-- __main__.py ........... Main script
  |  |-- algo_calc_flux.py ..... Algorithm
  |  |-- command_line.py ....... Command-line interface
  |  |-- ecmwf.py .............. Wrapper for ECMWF CDS API (only used when option --pressurelevel is called)
  |  |-- functions.py .......... A few functions needed by the algorithm
  |  |-- plot.py ............... Plot function (only used when option --output_file is called)
  |-- tests/ ................... Integrity tests
  |-- .gitignore ............... Files and directories not tracked by git
  ```


## Installation

### Dependencies

Mandatory dependencies:
```code
Python (version >=3.10)
Pandas
Numpy
Scipy
Pytest
Matplotlib
```

Optional dependencies:
```code
holoviews
netcdf4
xarray
cdsapi
```

### Simple installation procedure (pip)

Install package (and all its dependencies) with full functionalities:
```console
pip install 'so2-flux-calculator[dev]' --index-url https://git.icare.univ-lille.fr/api/v4/projects/661/packages/pypi/simple
```

Alternatively, install package by explicitly listing dependencies:
```console
pip install 'so2-flux-calculator[test,interactive,ecmwfapi]' --index-url https://git.icare.univ-lille.fr/api/v4/projects/661/packages/pypi/simple
```


### Installation procedure for developers (git+conda)


#### Download source

Manually download and unzip the project archive, or type:
```console
git clone https://git.icare.univ-lille.fr/icare-public/so2-flux-calculator.git
```

#### Install Python with Conda (optional)

1. Download miniconda3 installation script `Miniconda3-latest-Linux-x86_64.sh` from [https://docs.conda.io/projects/miniconda/en/latest/](https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh) (or choose another distribution from [this list](https://docs.conda.io/projects/miniconda/en/latest/)).

2. Move to the download directory, open a terminal and install the package using the installation script:
```console
bash Miniconda3-latest-Linux-x86_64.sh -p $INSTALL_DIR_CONDA
```
where `$INSTALL_DIR_CONDA` is the directory where you want to install conda. You need to have write permissions for this directory. Then, follow the instruction for installation. Be aware that conda will inflate rapidly, so that some disk space will be needed for this directory (typicaly, a few GB).

3. Add conda to path:
```console
export PATH=$INSTALL_DIR_CONDA/bin:$PATH
```

#### Setup a Conda environment (optional)

The file `./environment.yml` helps you creating an environment with all the packages necessary for running `so2_flux_calculator`. You can setup the environment from the command line. This will allow you to use the `--no-deps` option at `pip install` stage, since all the required packages will already be available in the environment (see below).

 1. Move to the folder containing the project files (hereafter named `$PACKAGE_DIR`) and open the terminal:
 ```console
cd $PACKAGE_DIR
```

 2. Run the following command to install all necessary packages within a conda environment:
 ```console
conda env create -f environment.yml
```

  <!-- *Not recommended*: alternatively, if you prefer to install all the packages into an already existing conda environment named `myenv`, you may use:
  ```console
  conda env update --name myenv -f environment.yml --prune
  ``` -->

 3. Activate the environment from the command line:
```console
conda deactivate
conda activate fluxcalculator
```

4. In case you broke something in the environment, you may remove the environment using:
```console
conda deactivate
conda remove --name fluxcalculator --all
```

#### Install the package and command-line tool

The file `./pyproject.toml` contains package metadata and parameters that facilitate installation.

1. Move to the folder containing containing the project files, open the terminal, and activate the environment from the command line:
```console
cd $PACKAGE_DIR
conda deactivate
conda activate fluxcalculator
```

2. Install the `so2_flux_calculator` executable into your conda environment using:
```console
pip install -e . --no-deps
```
<!-- Alternatively, if you prefer to install the package in a custom directory:
```console
pip install . -t $INSTALL_DIR --no-deps
```
  where `$INSTALL_DIR` is the directory where you want to install `so2_flux_calculator`. You need to have write permissions for this directory. You also need to update paths manually:
  ```console
  export PATH=$INSTALL_DIR/bin:$PATH
  export PYTHONPATH=$INSTALL_DIR:$PYTHONPATH
  ``` -->
  *Note*: The `--no-deps` option tells `pip` to ignore installation of dependencies listed in `./pyproject.toml`. This assumes that these dependencies are already installed in your environment. You may remove this option if you find that some packages are missing.


  3. If you need to remove the installation, use:
  ```console
  pip uninstall so2_flux_calculator
  ```

  4. Alternatively, install package from remote source using pip, optionally between brackets specifying which optional packages you'd like to install:

  ```console
  pip install 'so2-flux-calculator[test,interactive,ecmwfapi]' --index-url https://git.icare.univ-lille.fr/api/v4/projects/661/packages/pypi/simple

  ```

#### Test

1. Test the executable visibility:
```console
which so2_flux_calculator
```
This command should normally return the path to the executable, which should include your conda environment `bin/` directory. If the command returns `so2_flux_calculator not found`, it means that your environment variable `$PATH` is not setup correctly. Make sure your conda environment is activated. If nothing works, start again from scratch or seek help.

2. Test the package import:
```console
python -c "import so2_flux_calculator; print(so2_flux_calculator.__file__);"
```
This command should return the path to the python files of the package. If the command returns `No module named 'so2_flux_calculator'`, then something is probably wrong with your `sys.path`. The `sys.path`  should normally include `$PACKAGE_DIR/src`. You can display `sys.path` using the terminal command `python -m site`. A workaround consists in manually adding `$PACKAGE_DIR/src` to your environment variable `$PYTHONPATH`, but this should not be necessary. If this still doesn't work, start again from scratch or seek help.

3. Test the integrity of results on two sample datasets:
```console
cd $PACKAGE_DIR
python -m pytest ./tests
```

<!-- ### Optional installs

#### Install optional packages

You may want to unlock the following optional features of `so2_flux_calculator`:
*  use ECMWF API to automatically download ERA-5 wind speeds
* display results in interactive plots using [Holoviews](https://holoviews.org/)

The following procedure installs optional packages (`holoviews`, `netcdf4`, `xarray` and `cdsapi`) in one step:

1. Activate your environment:
```console
conda activate fluxcalculator
```

2. Add all optional packages to this environment, if they are not already there:
```console
conda env update -f environment.yml --prune
```
  Note: the above assumes that you use the default environment name `fluxcalculator`.

3. Alternatively, if updating the environment takes forever, or if you messed up with your environment names, you may remove the corrupted environment and create a fresh environment with :
  ```console
  conda deactivate
  conda remove --name fluxcalculator --all
  conda env create -f environment.yml
  conda activate fluxcalculator
  ``` -->


#### Setup ECMWF API  (optional)

Optionally, plume speed can be determined using wind-fields derived from ECMWF ERA-5 reanalysis. To setup the ECWF API as part of your conda environment, follow instructions at [https://cds.climate.copernicus.eu/api-how-to](https://cds.climate.copernicus.eu/api-how-to). The main steps are summarized below.


Setup your credentials for the ECMWF API, by copying the following code to a file named `$HOME/.cdsapirc`:
```bash
url: https://cds.climate.copernicus.eu/api/v2
key: {uid}:{api-key}
```
where `$HOME` is your home directory, and `$uid` and `$api-key` are respectively the user ID and API key provided by ECMWF at registration.

  The ECMWF registration form can be found at : [https://cds.climate.copernicus.eu/user/register?](https://cds.climate.copernicus.eu/user/register?).

## Usage

### Command line help

>  python so2_flux_calculator -h

```console
usage: so2_flux_calculator [-h] [--wind_speed WIND_SPEED]
   [--pressurelevel {1,2,3,5,7,10,20,30,50,70,100,125,150,175,200,225,250,300,350,400,450,500,550,600,650,700,750,775,800,825,850,875,900,925,950,975,1000}]
   [--confidence_detection {[0.0,100.0]}]
   [--cloud_fraction_file CLOUD_FRACTION_FILE]
   [--method {curvefit,polyfit,algebraic,algebraic2}]
   [--drop_distances [{25,50,75,100,125,150,175,200,250,300,350,400,500,1000} ...]]
   [--utc_time UTC_TIME] [--interactive_figure] [--output_file OUTPUT_FILE]
   [--output_directory OUTPUT_DIRECTORY] [--verbose]
   input_file

Compute SO<sub>2</sub> flux from integrated masses around volcano. Please use input file provided by the VolcPlume portal.
Developed primarily for TROPOMI.

positional arguments:
  input_file            Specify the input file (.csv format)
  cloud_fraction_file   Cloud fraction input file (.csv format)

options:
  --wind_speed WIND_SPEED
                        Wind speed (m/s)
  --pressurelevel {1,2,3,5,7,10,20,30,50,70,100,125,150,175,200,225,250,300,350,400,450,500,550,600,650,700,750,775,800,825,850,875,900,925,950,975,1000}
                        Pressure level (hPa)
  --altitude ALTITUDE   Altitude (km)
  --confidence_detection {[0.0,100.0]}
                        Percentage of confidence of statistical test for detection
  --method {curvefit,polyfit,algebraic,algebraic2}
                        Inversion method
  --drop_distances [{25,50,75,100,125,150,175,200,250,300,350,400,500,1000} ...]
                        List of distances to ignore in inversion
  --utc_time UTC_TIME   UTC time of acquisitions
  --interactive_figure  Interactive plot
  --output_file OUTPUT_FILE
                        Output file name (without extension). Will export results to a .csv file and plot results
                        in a .png file. Files are written to output directory.
  --output_directory OUTPUT_DIRECTORY
                        Output directory
  --verbose             Verbose
  --version             Version of code (git hash of executable)
  -h, --help            Show this help message and exit.
  ```

### How to set wind speed

There are three mutually exclusive arguments to set up the wind speed:

* `--wind_speed WIND_SPEED` (unit: m/s): fixed wind speed.

* `--pressurelevel PRESSURE` (unit: hPa): query ECMWF ERA-5 to extract wind speed at specified pressure level. Pressure level must be chosen in the list of 37 pressure levels proposed by ERA-5.

* `--altitude ALTITUDE` (unit: km): same as `--pressure`, except that a lookup table is used to convert altitude into pressure prior to ECMWF ERA-5 query.


### Examples

To facilitate reproduction of examples below, sample input files are available in the package directory `sample_inputs/`.

* ***Example 1***: Compute SO<sub>2</sub> flux at Piton de la Fournaise, from 15 September 2021 to 13 September 2023, using sample input data files provided with the package, with fixed wind speed of 6 m/s, excluding radius 1000 km:

```console
so2_flux_calculator \\
 $PACKAGE_DIR/sample_inputs/Piton/TROPOMI_SO2-7km_mass_25-50-75-100-150-200-250-300-400-500-1000km_Fournaise-Piton-de-la_du-0_qa-0_sza-90_tracks-7_2021-09-15_2023-09-14.csv \\
 $PACKAGE_DIR/sample_inputs/Piton/TROPOMI_CloudFraction_mean_25-50-75-100-150-200-250-300-400-500-1000km_Fournaise-Piton-de-la_qa-0_sza-90_tracks-7_2021-09-15_2023-09-13.csv \\
 --drop_distances 1000 --wind_speed 6
```

* ***Example 2***: Compute SO<sub>2</sub> flux at Etna, from 1 January to 31 December 2021, using sample input data files provided with the package, with variable wind speed deduced from ECMWF ERA-5 wind field extracted at 600 hPa pressure level (requires ECMWF CDS API):

```console
so2_flux_calculator \\
 $PACKAGE_DIR/sample_inputs/Etna/TROPOMI_SO2-7km_mass_25-50-75-100-150-200-250-300-400-500-1000km_Etna_du-0_qa-0_sza-90_tracks-22_2021-01-01_2021-12-31.csv \\
 $PACKAGE_DIR/sample_inputs/Etna/TROPOMI_CloudFraction_mean_25-50-75-100-150-200-250-300-400-500-1000km_Etna_qa-0_sza-90_tracks-22_2021-01-01_2021-12-31.csv \\
  --pressurelevel 600
```

### Export results

To save the result into a .csv file, and make a plot in .png format, add options `--output_file` and `--output_directory`.

Following example \#1 above:

```console
so2_flux_calculator \\
 $PACKAGE_DIR/sample_inputs/Piton/TROPOMI_SO2-7km_mass_25-50-75-100-150-200-250-300-400-500-1000km_Fournaise-Piton-de-la_du-0_qa-0_sza-90_tracks-7_2021-09-15_2023-09-14.csv \\
 $PACKAGE_DIR/sample_inputs/Piton/TROPOMI_CloudFraction_mean_25-50-75-100-150-200-250-300-400-500-1000km_Fournaise-Piton-de-la_qa-0_sza-90_tracks-7_2021-09-15_2023-09-13.csv \\
--drop_distances 1000 --wind_speed 6 \\
--output_file TROPOMI_SO2-7km_flux_25-50-75-100-150-200-250-300-400-500-1000km_Fournaise-Piton-de-la_du-0_qa-0_sza-90_tracks-7_2021-09-15_2023-09-13_6mps_drop-1000_standalone.png \\
--output_directory $HOME/test_so2_flux_calculator
```


The plot should look like [Figure 1](#figure1).

[figure1]: doc/TROPOMI_SO2-7km_flux_25-50-75-100-150-200-250-300-400-500-1000km_Fournaise-Piton-de-la_du-0_qa-0_sza-90_tracks-7_2021-09-15_2023-09-13_6mps_drop-1000_standalone.png#center "Figure 1"
![Figure 1][figure1]<br>*[Figure 1](#figure1): Example of output static plot on Piton de la Fournaise*


The .csv file contains the time-series of fluxes, as well as other information deduced from the calculation:

 *  `flux` : SO<sub>2</sub> flux [kton/day]

 *  `sigma_flux` : 1-sigma uncertainty on SO<sub>2</sub> flux [kton/day]

 *  `sigma_CA_post` : estimated pixel noise affecting the input data [DU]

 *  `degassing` : True if degassing is positively detected, False if detection is negative

 *  `a`, `b`, `c` : estimated a posteriori values of the parameters of the regression (respectively, intercept, linear and quadratic terms)

 *  `sigma_a`, `sigma_b`, `sigma_c` : a posteriori 1-sigma uncertainties for `a`, `b`, `c`


### Interactive mode

An interactive mode allows for filtering outputs using two adjustable criteria:
* maximum cloud fraction (0-100%)
* maximum areal fraction of image affected by swath edge pixels (0-100%)

This mode requires that the user supplies an ancillary input file containing the cloud fractions for every radius. This file must have exactly the same dates as the main input file.

To activate the interactive mode, use the optional argument `--interactive`.

The command opens a tab in your web browser. The sliders at the upper left corner allow for interactively adjusting the two criteria (`Max % cloud fraction` and `Max % on edge track`, respectively).

The resulting time-series can be exported to a .csv file by clicking on the button "`Export results`".

Following example \#2 above, [Figure 2](#figure2) displays the result obtained after adding `--interactive`.

[figure2]: doc/TROPOMI_SO2-7km_flux_25-50-75-100-150-200-250-300-400-500-1000km_Etna_du-0_qa-0_sza-90_tracks-22_2021-01-01_2021-12-31_0600hPa_standalone_capture.png#center "Figure 2"
![Figure 2][figure2]<br>*[Figure 2](#figure2): Example of output reactive plot using the option `interactive` on Etna.*



## Reproducing figures from the paper

The figures of the paper can also be reproduced using sample input files, available from the EaSY Data repository from [https://doi.org/10.57932/235f8c42-142b-40ee-9948-518e83554a7d](www.easydata.earth/#/public/metadata/235f8c42-142b-40ee-9948-518e83554a7d):

```cite
Grandin, R., Boichu, M., Mathurin, T., & Pascal, N. (2024b).
Sulfur Dioxide emissions from Etna and Piton de la Fournaise volcanoes (2021-2023)
from Sentinel-5P/TROPOMI [Dataset].
Retrieved from : https://www.easydata.earth/#/public/metadata/235f8c42-142b-40ee-9948-518e83554a7d
doi: 10.57932/235f8c42-142b-40ee-9948-518e83554a7d
```

The commands for reproducing Figures 4 and 6 of the paper are provided below. Note that these commands require that the ECMWF API has been correctly configured beforehand. If you are unable to setup the ECMWF API, daily wind velocity at the prescribed pressure level are available in the directories `input_wind_speed`. Simply paste these NetCDF files into the output directory (here, `output_flux`), and re-run the command.

* **Etna (Figure 4)**

```code
so2_flux_calculator \
--pressure 600 \
Etna/input_mass/TROPOMI_SO2-7km_mass_25-50-75-100-150-200-250-300-400-500-1000km_Etna_sza-90_tracks-22_du-0_qa-0_2021-01-01_2021-12-31.csv \
Etna/input_cloud_fraction/TROPOMI_CloudFraction_mean_25-50-75-100-150-200-250-300-400-500-1000km_Etna_qa-0_sza-90_tracks-22_2021-01-01_2021-12-31.csv \
--output_file \
TROPOMI_SO2-7km_flux_25-50-75-100-150-200-250-300-400-500-1000km_Etna_sza-90_tracks-22_du-0_qa-0_2021-01-01_2021-12-31_0600hPa \
--output_directory output_flux \
--verbose
```

* **Piton de la Fournaise (Figure 6)**

```code
so2_flux_calculator
--pressure 700 \
--drop_distances 1000 \
Piton_de_la_Fournaise/input_mass/TROPOMI_SO2-7km_mass_25-50-75-100-150-200-250-300-400-500km_Fournaise-Piton-de-la_sza-90_tracks-7_du-0_qa-0_2021-09-15_2023-09-14.csv \
Piton_de_la_Fournaise/input_cloud_fraction/TROPOMI_CloudFraction_mean_25-50-75-100-150-200-250-300-400-500km_Fournaise-Piton-de-la_sza-90_tracks-7_qa-0_2021-09-15_2023-09-14.csv \
--output_file \
TROPOMI_SO2-7km_flux_25-50-75-100-150-200-250-300-400-500km_Fournaise-Piton-de-la_sza-90_tracks-7_du-0_qa-0_2021-09-15_2023-09-14_0700hPa_max500km \
--output_directory output_flux \
--verbose
```

## Contributors

* Raphaël Grandin, Institut de physique du globe de Paris, Université Paris Cité [grandin \<at\> ipgp.fr]

* Marie Boichu, Laboratoire d’Optique Atmosphérique, Université de Lille, CNRS

* Théo Mathurin, ICARE, Université de Lille, AERIS

* Nicolas Pascal, ICARE, Université de Lille, AERIS


## Licence

THIS IS RESEARCH CODE PROVIDED TO YOU "AS IS" WITH NO WARRANTIES OF CORRECTNESS. USE AT YOUR OWN RISK.

This software is open source under the terms of the the MIT License. Please read the accompanying `LICENSE` file.

## Documentation

PDF automatically generated with pandoc:
```
pandoc --include-in-header doc/README_listings-setup.tex \
--listings \
-f markdown-implicit_figures \
--pdf-engine=xelatex \
-V papersize:a4 \
-V urlcolor=cyan \
-V geometry:hmargin=2cm -V geometry:vmargin=2cm \
-V 'mainfont:OpenSans-Regular' \
-V 'mainfontoptions:Extension=.ttf, UprightFont=*, BoldFont=OpenSans-Bold, ItalicFont=OpenSans-Italic, BoldItalicFont=OpenSans-BoldItalic' \
-V 'sansfont:DejaVuSans.ttf' \
-V 'monofont:DejaVuSansMono.ttf' \
-V 'mathfont:texgyredejavu-math.otf' \
README.md -o README.pdf
```


---
title: SO$_2$ Flux Calculator
subtitle: Python implementation of the volcanic SO$_2$ flux calculator add-on of the VolcPlume platform
author:
    - Raphael Grandin
    - Marie Boichu
    - Théo Mathurin
    - Nicolas Pascal
date: \today{}

---
