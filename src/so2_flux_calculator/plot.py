
from .__constants__ import *

# Vertical cross-hair
# Adapted from https://discourse.holoviz.org/t/linked-crosshair/4326

# def plot_data_results_interactive(df_data, df_results, df_cf=None, dateutcnow=None):
#
#     import numpy as np
#     import holoviews as hv
#     from holoviews import dim
#     import panel as pn
#
#     hv.extension('bokeh')
#
#
#     threshold = pn.widgets.IntSlider(start=10, end=90, value=50,
#                                      name='minimum x value')
#
#     scatter = hv.Scatter((np.arange(100), np.random.randn(100)*100),
#                          kdims=['x'], vdims=['y'])
#
#     def get_plot(threshold):
#         return scatter.select(dim('y') >= threshold)
#     plot = hv.DynamicMap(pn.bind(get_plot, threshold=threshold))
#
#     pn.Column(threshold, plot).show()
#     #pn.Row(scatter.select()).show();

def plot_data_results_interactive(df_data, df_results, output_directory, output_file, dateutcnow, comput_info, export_dict):

    # For visualization (only necessary for this notebook)
    import holoviews as hv
    from holoviews import dim
    from holoviews import opts
    import panel as pn
    import numpy as np
    import pandas as pd
    import os
    from io import StringIO
    pn.extension()
    from bokeh.models import CrosshairTool, Span
    hv.extension('bokeh')
    opts.defaults(opts.Scatter(size=10))

    title = "%s (%.2f°N-%.2f°E) %s/%s" % (comput_info['volcano'].replace(' ', '-'), comput_info['longitude'], comput_info['latitude'], comput_info['start'], comput_info['end'])

    # make crosshair object
    linked_crosshair = CrosshairTool(overlay=[Span(dimension="width"), Span(dimension="height")])

    tools=['hover', linked_crosshair]
    opts_scatter = {"tools": tools, "width": 800, "height": 150, "size": 8}#, 'line_dash': 'dotted'}
    opts_curve = {"tools": tools, "width": 800, "height": 150, "line_width": 1, "line_dash": 'dotted'}#, 'line_dash': 'dotted'}

    list_radii = df_data.index.levels[1].values

    hv_scatter_overlay_mass = hv.NdOverlay({radius : hv.Scatter(np.maximum(1e-2, df_data[comput_info['variable']]).loc(axis=0)[:,radius].droplevel(1), vdims=[(comput_info['variable'], 'SO2 mass\n(kton)')]) for radius in list_radii}).opts(opts.Scatter(**opts_scatter))
    hv_curve_overlay_mass = hv.NdOverlay({radius : hv.Curve(np.maximum(1e-2, df_data[comput_info['variable']]).loc(axis=0)[:,radius].droplevel(1), vdims=[(comput_info['variable'], 'SO2 mass\n(kton)')]) for radius in list_radii}).opts(opts.Curve(**opts_curve, logy=True))

    vdims = [('flux', 'Flux\n(kton/day)'), ('speed', 'Wind speed\n(m/s)'), ('degassing', 'Degassing'),
                 ('sigma_flux', 'Standard deviation\n(kton/day)'), ('sigma_CA_post', 'Pixel\nstandard deviation (DU)'),
                 ('TROPOMI_CloudFraction_mean', 'Cloud Fraction (%)'), ('fraction_filtered_edge_tracks', 'Fraction filtered\non edge tracks (%)')]

    hv_scatter_flux = hv.Scatter(df_results, kdims='time', vdims=vdims).opts(**opts_curve, color=hv.dim('degassing').categorize({True: 'red', False: 'black'}))
    hv_curve_flux = hv.Curve(df_results, kdims='time', vdims=vdims).opts(opts.Curve(**opts_curve, color='black', logy=False))

    hv_errorbar_flux = hv.ErrorBars(df_results, kdims='time', vdims=['flux', 'sigma_flux'])
    hv_errorbar_flux.opts(opts.ErrorBars(color='grey'))

    hv_scatter_sigmaCA = hv.Scatter(df_results, kdims='time', vdims=[('sigma_CA_post', 'Pixel standard-\ndeviation (DU)'), 'TROPOMI_CloudFraction_mean', 'fraction_filtered_edge_tracks']).opts(opts.Scatter(**opts_scatter, logy=False))
    hv_curve_sigmaCA = hv.Curve(df_results, kdims='time', vdims=[('sigma_CA_post', 'Pixel standard-\ndeviation (DU)')]).opts(opts.Curve(**opts_curve, color='blue', logy=False))

    hv_scatter_overlay_cf = hv.Scatter(df_results, kdims='time', vdims=[('TROPOMI_CloudFraction_mean', 'Cloud Fraction\n(%)')]).opts(opts.Scatter(**opts_scatter, color='lightblue'))
    hv_curve_overlay_cf = hv.Curve(df_results, kdims='time', vdims=[('TROPOMI_CloudFraction_mean', 'Cloud Fraction\n(%)')]).opts(opts.Curve(**opts_curve, color='lightblue', logy=False))

    hv_scatter_edge_track = hv.Scatter(df_results, kdims='time', vdims=[('fraction_filtered_edge_tracks', 'Fraction filtered\non edge tracks (%)')]).opts(opts.Scatter(**opts_scatter, color='orange'))
    hv_curve_edge_track = hv.Curve(df_results, kdims='time', vdims=[('fraction_filtered_edge_tracks', 'Fraction filtered\non edge tracks (%)')]).opts(opts.Curve(**opts_curve, color='orange'))

    hv_scatter_wind_speed = hv.Scatter(df_results, kdims='time', vdims=[('speed', 'Wind speed\n(m/s)')]).opts(opts.Scatter(**opts_scatter, color='green'))
    hv_curve_wind_speed = hv.Curve(df_results, kdims='time', vdims=[('speed', 'Wind speed\n(m/s)')]).opts(opts.Curve(**opts_curve, color='green'))

    sidebar_width = 300
    sidebar_margin = 40

    threshold_cf = pn.widgets.EditableIntSlider(start=0, end=100, value=100,
                                 name='Max % cloud fraction', width=200)
    threshold_edge = pn.widgets.EditableIntSlider(start=0, end=100, value=100,
                                 name='Max % on edge track', width=200)


    def get_plot_flux(threshold_cf, threshold_edge):
        return hv_scatter_flux.select((dim('TROPOMI_CloudFraction_mean') <= threshold_cf)*(dim('fraction_filtered_edge_tracks') <= threshold_edge))
    def get_plot_sigmaCA(threshold_cf, threshold_edge):
        return hv_scatter_sigmaCA.select((dim('TROPOMI_CloudFraction_mean') <= threshold_cf)*(dim('fraction_filtered_edge_tracks') <= threshold_edge))
    def get_plot_cf(threshold_cf):
        return (hv.HLine(threshold_cf).opts(color='lightblue', alpha=0.7))*hv_scatter_overlay_cf.select(dim('TROPOMI_CloudFraction_mean') <= threshold_cf)
    def get_plot_edge(threshold_edge):
        return (hv.HLine(threshold_edge).opts(color='orange', alpha=0.7))*hv_scatter_edge_track.select(dim('fraction_filtered_edge_tracks') <= threshold_edge)

    hv_scatter_flux_interactive = hv.DynamicMap(pn.bind(get_plot_flux, threshold_cf=threshold_cf, threshold_edge=threshold_edge))
    hv_scatter_sigmaCA_interactive = hv.DynamicMap(pn.bind(get_plot_sigmaCA, threshold_cf=threshold_cf, threshold_edge=threshold_edge))
    hv_scatter_overlay_cf_interactive = hv.DynamicMap(pn.bind(get_plot_cf, threshold_cf=threshold_cf))
    hv_scatter_edge_track_interactive = hv.DynamicMap(pn.bind(get_plot_edge, threshold_edge=threshold_edge))

    pn_flux = pn.Card(hv_curve_flux*hv_errorbar_flux*hv_scatter_flux_interactive, title='Flux')
    pn_sigmaCA = pn.Card(hv_curve_sigmaCA*hv_scatter_sigmaCA_interactive, title='Pixel noise')
    pn_cf = pn.Card(hv_curve_overlay_cf*hv_scatter_overlay_cf_interactive, title='Cloud fraction')
    pn_edge = pn.Card(hv_curve_edge_track*hv_scatter_edge_track_interactive, title='Percentage edge swath')
    pn_windspeed = pn.Card(hv_curve_wind_speed*hv_scatter_wind_speed, title='Wind speed')
    pn_mass = pn.Card(hv_curve_overlay_mass*hv_scatter_overlay_mass, title='Mass')

    pn_slidebars = pn.Card(
        pn.Column(
            threshold_cf,
            threshold_edge
            ),
        width=sidebar_width-sidebar_margin,
        title="Thresholds"
        )

    # pn_visible = pn.layout.FloatPanel(
    #         pn.Row(pn.widgets.StaticText(value='Flux', width=130), pn_flux.controls(['visible'])[1]),
    #         pn.Row(pn.widgets.StaticText(value='Pixel noise', width=130), pn_sigmaCA.controls(['visible'])[1]),
    #         pn.Row(pn.widgets.StaticText(value='Cloud fraction', width=130), pn_cf.controls(['visible'])[1]),
    #         pn.Row(pn.widgets.StaticText(value='Perc. edge swath', width=130), pn_edge.controls(['visible'])[1]),
    #         pn.Row(pn.widgets.StaticText(value='Wind speed', width=130), pn_windspeed.controls(['visible'])[1]),
    #         name="Toggle plot visibility",
    #         contained=False, position='left-center', offsetx=1000
    #         )
    pn_visible = pn.Card(
        pn.Column(
            pn.Row(pn.widgets.StaticText(value='Flux', width=120), pn_flux.controls(['visible'])[1]),
            pn.Row(pn.widgets.StaticText(value='Pixel noise', width=120), pn_sigmaCA.controls(['visible'])[1]),
            pn.Row(pn.widgets.StaticText(value='Cloud fraction', width=120), pn_cf.controls(['visible'])[1]),
            pn.Row(pn.widgets.StaticText(value='Perc. edge swath', width=120), pn_edge.controls(['visible'])[1]),
            pn.Row(pn.widgets.StaticText(value='Wind speed', width=120), pn_windspeed.controls(['visible'])[1]),
            pn.Row(pn.widgets.StaticText(value='Mass', width=120), pn_mass.controls(['visible'])[1]),
            ),
        width=sidebar_width-sidebar_margin,
        title="Toggle plot visibility"
        )

    pn_result = pn.Column(
        pn_flux,
        pn_sigmaCA,
        pn_cf,
        pn_edge,
        pn_windspeed,
        pn_mass)

    close_button = pn.widgets.Button(name='Exit', button_type='success')
    close_button.js_on_click(code="""window.close(``, `_parent`, ``);""")

    def save_filtered_data_to_file():
        sio = StringIO()
        pd.DataFrame.from_dict(export_dict, orient="index").T.to_csv(sio, index=False)
        sio.write(f'Post-processing filters: max_cf=%.2f ; max_edge=%.2f\n' % (threshold_cf.value, threshold_edge.value))
        mask_cf = (df_results['TROPOMI_CloudFraction_mean']<=threshold_cf.value).values
        mask_edge = (df_results['fraction_filtered_edge_tracks']<=threshold_edge.value).values
        df_results[mask_cf*mask_edge].to_csv(sio, index=False)
        sio.seek(0)
        return sio

    save_button = pn.widgets.FileDownload(label='Export results', button_type='success',
        filename='flux_export_data.csv',
        callback=save_filtered_data_to_file
        )

    top_dir = os.path.dirname(os.path.dirname(__file__))

    #pn.layout.HSpacer()

    logo = pn.pane.PNG(os.path.join(top_dir, 'assets', 'favicon.png'), width=30)
    header = [pn.Row(pn.pane.Str(title, margin=(0, 0, 2, 10),
                             styles={'font-size': '8pt', 'font-style': 'italic', 'color': '#f1afc9'}, align='end'),
                 pn.layout.HSpacer(), logo, close_button)]

    template = pn.template.VanillaTemplate(
        title='Flux Calculator (offline version)',
        sidebar=[
            pn_slidebars,
            pn_visible,
            save_button
            ],
        header = header,
        header_background='#5F1834',
        sidebar_width=sidebar_width,
        favicon=os.path.join(top_dir, 'assets', 'favicon.png')
    )
    template.main.append(pn_result)

    template.show()

def plot_data_results(df_data, df_results, output_directory, output_file, dateutcnow, comput_info, verbose=False):

    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import matplotlib.dates as mdates
    import os

    save_figure = os.path.join(output_directory, output_file+'.png')
    if verbose:
        print("Plotting result to %s" % save_figure)

    title = "%s (%.2f°N-%.2f°E) %s/%s" % (comput_info['volcano'].replace(' ', '-'), comput_info['longitude'], comput_info['latitude'], comput_info['start'], comput_info['end'])

    # Days with degassing detection
    mask_detect = (df_results['degassing'].fillna(False))

    fig, axs = plt.subplots(nrows=3, ncols=1, sharex=True, figsize=(12,10))
    for distance in list_distances:
        if len(df_data.iloc[df_data.index.get_level_values('radius') == distance]>0):
            axs[0].plot(df_data.iloc[df_data.index.get_level_values('radius') == distance].droplevel('radius')[comput_info['variable']], label='r=%dkm' % distance)

    axs[1].plot(df_results['flux'], '-', c='grey')
    if len(~mask_detect>0):
        axs[1].errorbar(x=df_results[~mask_detect.values].index, y=df_results[~mask_detect.values]['flux'], yerr=df_results[~mask_detect.values]['sigma_flux'], capsize=5, ls='none', c='grey')
        axs[1].plot(df_results[~mask_detect.values]['flux'], 'o', c='k', ms=6, label='No degassing detected')
    if len(mask_detect>0):
        axs[1].errorbar(x=df_results[mask_detect.values].index, y=df_results[mask_detect.values]['flux'], yerr=df_results[mask_detect.values]['sigma_flux'], capsize=5, ls='none', c='darkred')
        axs[1].plot(df_results[mask_detect.values]['flux'], 'o', c='r', ms=6, label='Degassing detected')
    axs[0].set_ylabel('SO$_2$ mass (kton)')
    axs[0].legend(ncol=3)
    axs[0].set_yscale('log')

    axs[1].set_ylabel('SO$_2$ flux (kton/day)')
    axs[1].legend()
    #axs[1].set_yscale('log')
    #axs[1].set_ylim(bottom=1e-1, top=df_results['flux'].max()*2)
    axs[1].xaxis.set_major_formatter(
        mdates.ConciseDateFormatter(axs[1].xaxis.get_major_locator()))

    axs[2].plot(df_results['sigma_CA_post'], '-o', color='b')
    axs[2].set_ylabel('Pixel noise (DU)')
    axs[2].set_ylim(bottom=-1e-2)

    for ax in axs:
        ax.text(0.99, 0.02, 'Made with TROPOMI so2_flux_calculator\n%s' % dateutcnow , transform=ax.transAxes,
            fontsize=6, color='gray', alpha=0.5,
            ha='right', va='bottom')

    axs[0].set_title(title)

    plt.tight_layout()
    plt.savefig(save_figure)
