
import numpy as np
from scipy.optimize import curve_fit
from scipy.optimize import minimize as scpy_minimize
from scipy.stats import norm as scpy_n

from .functions import *

# Predict mean and variance on SO2 mass in circular domain due to noise only, given cutoff on CA
def predict_mass(CAmin=0.25, mu_CA=0, sigma_CA=0.25, r=1000, pixel_area=24.500000, du_to_kton=0.0007002642684658869):
    # Predict summed SO2 mass in a circle of radius R (unit: km)
    # assuming that mass is only due to noise, described as
    # a truncated random variable (truncated below CAmin)
    # with zero-mean.
    # Inputs:
    #  * CAmin : minimum CA used for truncation (unit: DU)
    #  * mu_CA : mean of normal pdf of pixels prior to truncation (unit: DU)
    #  * sigma_CA : standard deviation of normal pdf of pixels (unit: DU)
    #  * r : radius (km)
    #  * pixel_area : pixel area (km2)
    #  * du_to_kton : factor for converting DU to kton (tip: set it to 1 to get a result in DU)
    # Outputs :
    # - expectation on mass (unit: kton)
    # - variance on mass (unit: kton)
    if sigma_CA == 0.:
        sigma_CA = 1.e-7
    alpha = (CAmin - mu_CA) / sigma_CA
    fraction = 1-fF(alpha)
    E = mu_CA + sigma_CA*fZ(alpha)
    s2 = sigma_CA**2 * (1 - fdelta(alpha))
    nb_pix = (np.pi*r**2/(pixel_area))

    # Expectation
    EYn = np.nan_to_num(fraction*E*du_to_kton*nb_pix)
    # Variance
    VarYn = np.nan_to_num(fraction*s2*du_to_kton*nb_pix)

    return EYn, VarYn

# Compute flux from masses integrated in circular domain and estimate noise, given cutoff on CA
def calc_flux_linquad_weighted(xydata, CAmin=0., sigma_CA=0.25, iter_max=4, pixel_area=24.500000, du_to_kton=0.0007002642684658869, method='curvefit', verbose=False, drop_distances=None):
    # Compute fluxes in the slender plume approximation
    # - with curvefit method
    # - with weights depending on radius
    # - with constraints on quadratic term
    # - with a posteriori evaluation of the level of noise in the data
    # Arguments:
    #  * x : list of distances (km)
    #  * y : list of masses (kton)
    #  * CAmin : thresholding applied to the pixels prior to summation (DU)
    #  * sigma_CA : a priori level of noise affecting pixels prior to summation (DU)
    #  * pixel_area : pixel area (km2)
    #  * du_to_kton : factor for converting DU to kton
    #  * iter_max : maximum number of iterations for a posterior evaluation of uncertainties
    #  * method : can be "curvefit", "polyfit", "algebraic" or "algebraic2"
    # Returns:
    #  * a0 : polynomial coefficients for the intercept component of the signal (unit: kton)
    #  * a1 : polynomial coefficients for the linear component of the signal (unit: kton/km)
    #  * a2 : polynomial coefficients for the quadratic component of the signal (unit: kton/km2)
    #  * sigma_0_post : a posteriori estimate of uncertainty on a0 (unit: kton)
    #  * sigma_1_post : a posteriori estimate of uncertainty on a1 (unit: kton/km)
    #  * sigma_2_post : a posteriori estimate of uncertainty on a2 (unit: kton/km2)
    #  * sigma_CA_post : a posteriori level of noise affecting pixels prior to summation (unit: DU)
    # Remarks:
    #  * "a0" (intercept) is unbounded
    #  * "a1" (linear term) may have non-negativity constraint (if method='curvefit').
    #  * "a2" (quadratic term) may have a lower bound fixed to "bound_min_a2" (if method='curvefit')

    # Fill X and Y
    x = xydata.index
    y = xydata.values
    if verbose:
        print("** Input (x): ", x)
        print("** Input (y): ", y)

    # Drop some distances, if needed
    if drop_distances is not None:
        xmask = []
        for dist in drop_distances:
            xmask.append(np.where(x == dist))
        x = np.array([_x for _x in x if _x not in x[xmask]])
        y = np.array([_y for _y in y if _y not in y[xmask]])

    if (method != 'curvefit') & (method != 'polyfit') & (method != 'algebraic') & (method != 'algebraic2'):
        method = 'curvefit'

    if np.isnan(y).sum(): # if any value is NaN, return nothing
        a0 = a1 = a2 = sigma_0_post = sigma_1_post = sigma_2_post = sigma_CA_post = tstar1_post = np.nan
    else:

        if verbose:
            print("** sigma_CA:", sigma_CA, "/ CAmin:", CAmin)

        # # # # # # # # # # # # # # # # # # # # # # # #
        # Step 1 : use a priori information (sigma_CA, CAmin)
        #
        #  - estimate a priori value for a2 ("prior_a2")
        #  - provide uncertainties on masses ("y_sigma")
        #  - define plausible lower bound on a2 ("bound_min_a2")

        sigma_CA_default = 0.1
        max_n_sigma_CA_default = 7 # /!\ should not be greater than 8 to prevent overflow in fZ.
        if sigma_CA < sigma_CA_default: # sigma_CA (prior) should not be smaller than sigma_CA_default (lower bound)
            sigma_CA = sigma_CA_default
        if CAmin > max_n_sigma_CA_default*sigma_CA_default:
            sigma_CA = CAmin/max_n_sigma_CA_default
            EYn, VarYn = predict_mass(r=x, sigma_CA=sigma_CA, CAmin=CAmin, pixel_area=pixel_area, du_to_kton=du_to_kton)
            bound_min_a2 = np.mean(predict_mass(r=x, sigma_CA=sigma_CA, CAmin=CAmin, pixel_area=pixel_area, du_to_kton=du_to_kton)[0]/x**2)
        else:
            EYn, VarYn = predict_mass(r=x, sigma_CA=sigma_CA, CAmin=CAmin, pixel_area=pixel_area, du_to_kton=du_to_kton)
            bound_min_a2 = np.mean(predict_mass(r=x, sigma_CA=sigma_CA_default, CAmin=CAmin, pixel_area=pixel_area, du_to_kton=du_to_kton)[0]/x**2)
        prior_a2 = np.mean(EYn/x**2)
        prior_a0 = 0. # intercept set to 0 a priori
        prior_a1 = 0. # linear term set to 0 a priori (no degassing a priori)
        if verbose:
            print("** prior_a2:", prior_a2, "/ bound_min_a2:", bound_min_a2)
            print("** EYn:", EYn, "/ VarYn:", VarYn )

        # # # # # # # # # # # # # # # # # # # # # # # #
        # Step 2 : weighted least squares estimate on 2nd order polynomial model with bounds
        #

        if method == 'curvefit':
            # popt contains the estimated values for a0, a1 and a2
            # pcov is the covariance matrix, used later to extract the uncertainties on estimated parameters
            # non-negative constraints is imposed on a1 (linear term)
            # lower bound is imposed on a2 at bound_min_a2 (quadratic term)
            # uncertainties on observations are provided as "sigma", and are used for weighting
            y_sigma = np.sqrt(VarYn)
            popt, pcov = curve_fit(fit_func_lin_quad_withintercept, x, y, sigma=y_sigma, absolute_sigma=True, bounds=([-1.e7, 0., bound_min_a2], [1.e7, 1.e5, 1.e5]))#, p0 = [prior_a0, prior_a1, prior_a2])
            a0, a1, a2 = popt # estimated parameters
            sigma_0, sigma_1, sigma_2 = np.sqrt(np.diag(pcov)) # posterior uncertainty
        elif method == 'polyfit':
            # regular, unconstrained inversion using polyfit (fits y = a2*r^2+a1*r+a0)
            # observations are weighted by 1/uncertainty
            y_sigma = np.sqrt(VarYn)
            popt, pcov = np.polyfit(x, y, 2, cov='unscaled', w=1/y_sigma)    # Fits y = a2*r^2+a1*r+a0, cov='unscaled' to extract covariance matrix without rescaling (see https://github.com/numpy/numpy/pull/11196 and https://github.com/numpy/numpy/pull/11197)
            a0, a1, a2 = popt[2], popt[1], popt[0] # /!\ order is reversed w.r.t. curvefit
            sigma_2, sigma_1, sigma_0 = np.sqrt(np.diag(pcov)) # posterior uncertainty
        elif method == 'algebraic':
            # regular, unconstrained inversion using algebraic formulas (least-squares)
            # observations are provided for every radius
            # uncertainties on observations are fed into a data covariance matrix
            # diagonal of posterior model covariance matrix represent posterior uncertainties
            n = len(y)
            y_n = (y/x).reshape(-1,1)
            x_n = (x).reshape(-1,1)
            y_sigma = np.sqrt(VarYn)/x
            iCd = np.diag(1/y_sigma**2)
            G = np.hstack((np.ones((n, 1)), x_n))
            iGtiCdG = np.linalg.inv(np.dot(G.T, np.dot(iCd,G)))
            betahat = np.dot(np.dot(np.dot(iGtiCdG, G.T),iCd),y_n)
            a0 = 0.; sigma_0=0.;
            a1, a2 = float(betahat[0]), float(betahat[1])
            sigma_1, sigma_2 = np.sqrt(np.diag(iGtiCdG)) # posterior uncertainty
        elif method == 'algebraic2':
            # regular, unconstrained inversion using algebraic formulas (least-squares)
            # observations are transformed to place them at the mid-point between each successive pair of circles
            # intercept is assumed equal to zero
            # uncertainties on observations are fed into a data covariance matrix
            # diagonal of posterior model covariance matrix represent posterior uncertainties
            n = len(y)
            y_n = np.hstack((y[0]/x[0], np.diff(y)/np.diff(x))).reshape(-1,1)
            x_n = np.hstack((x[0],(x[1:]+x[:-1]))).reshape(-1,1)
            y_sigma = np.sqrt(VarYn)
            y_sigma = (np.hstack((y_sigma[0],(y_sigma+np.roll(y_sigma, 1))[1:])).reshape(-1,1)/x_n).reshape(-1)
            iCd = np.diag(1/y_sigma**2)
            G = np.hstack((np.ones((n, 1)), x_n))
            iGtiCdG = np.linalg.inv(np.dot(G.T, np.dot(iCd,G)))
            betahat = np.dot(np.dot(np.dot(iGtiCdG, G.T),iCd),y_n)
            a0 = 0.; sigma_0=0.;
            a1, a2 = float(betahat[0]), float(betahat[1])
            sigma_1, sigma_2 = np.sqrt(np.diag(iGtiCdG)) # posterior uncertainty
        elif method == 'algebraic3':
            # regular, unconstrained inversion using algebraic formulas (least-squares)
            # observations are provided for every radius
            # uncertainties on observations are fed into a data covariance matrix
            # diagonal of posterior model covariance matrix represent posterior uncertainties
            n = len(y)
            y_n = (y).reshape(-1,1)
            x_n = (x).reshape(-1,1)
            y_sigma = np.sqrt(VarYn)
            iCd = np.diag(1/y_sigma**2)
            G = np.hstack((np.ones((n, 1)), x_n, x_n**2))
            iGtiCdG = np.linalg.inv(np.dot(G.T, np.dot(iCd,G)))
            betahat = np.dot(np.dot(np.dot(iGtiCdG, G.T),iCd),y_n)
            #a0 = 0.; sigma_0=0.;
            a0, a1, a2 = float(betahat[0]), float(betahat[1]), float(betahat[2])
            sigma_0, sigma_1, sigma_2 = np.sqrt(np.diag(iGtiCdG)) # posterior uncertainty

        if verbose:
            print("** a0: %5.3g" % a0, "/ sigma_0: %5.3g" % sigma_0)#, "/ sigma_0_mse: %5.3g" % sigma_0_mse)
            print("** a1: %5.3g" % a1, "/ sigma_1: %5.3g" % sigma_1)#, "/ sigma_1_mse: %5.3g" % sigma_1_mse)
            print("** a2: %5.3g" % a2, "/ sigma_2: %5.3g" % sigma_2)#, "/ sigma_2_mse: %5.3g" % sigma_2_mse)

        # # # # # # # # # # # # # # # # # # # # # # # #
        # Step 3 : re-estimate posterior uncertainties
        #

        # # Step 3a : use analytic formula

        # Solve analytically inverse problem relating pixellic error and quadratic coefficient, given truncation (CAmin)
        sigma_CA_post = inv_func_fw_scaled(CAmin, a2, pixel_area, du_to_kton)

        # Scale all other uncertainties accordingly
        ratio_sigma = sigma_CA_post / sigma_CA

        # Apply scaling
        sigma_0_post, sigma_1_post, sigma_2_post = [ratio_sigma * sigma for sigma in [sigma_0, sigma_1, sigma_2]]

        if verbose:
            print("** ratio_sigma:", ratio_sigma)
            print("** sigma_CA:", sigma_CA, "/ sigma_CA_post:", sigma_CA_post)
            print("** sigma_0_post: %5.3g" % sigma_0_post, "/ sigma_1_post: %5.3g" % sigma_1_post, "/ sigma_2_post: %5.3g" % sigma_2_post)

        # # Step 3b : use empirical estimate of uncertainty using residuals

        # Sum of squared errors
        sse = np.sum((y - fit_func_lin_quad_withintercept(x, a0, a1, a2))**2)

        # Degrees of freedom (neglect quadratic term and intercept)
        dof = len(y) - 2

        # Mean squared error = unbiased estimator of data variance
        mse = sse/dof

        # Mean of independent variable
        xmean = np.mean(x)

        # Variance of slope (neglect quadratic term and intercept)
        sigma_1_post_alternative = np.sqrt(mse / np.sum((x-xmean)**2))

        if verbose:
            print("** sse: %5.3g" % sse, "/ mse: %5.3g" % mse, "/ sigma_1_post_alternative %5.3g" % sigma_1_post_alternative)

        # # Step 3c : Final uncertainty on flux

        # Update to largest of two estimates
        sigma_1_post = np.maximum(sigma_1_post, sigma_1_post_alternative)

        tstar1_post = ((float(a1) - prior_a1) / sigma_1_post)

    return [a0, a1, a2, sigma_0_post, sigma_1_post, sigma_2_post, sigma_CA_post, tstar1_post]


def degassing_detected(tstar1_post, confidence_detection=99.5):
    # Step 4 : define objective criterion for detection of degassing
    #
    # returns: boolean with 'True' is volcanic degassing is detected, 'False' otherwise
    #
    # Test statistic for a1
    # Ho : a1 = 0 (no need to test negative values, because a1 has non-negativity constraints)
    # Will be re-evaluated a posteriori when sigma_1 has been updated

    # Define confidence threshold for the test
    freq_n = 1 - confidence_detection / 100.  # convert to probability threshold (between 0 and 1)
    pvalue_upper_cutoff_n = (sp.ndtri(1 - freq_n))  # CDF of normal distribution for that probability

    # Make the test, return boolean
    detected = tstar1_post > pvalue_upper_cutoff_n

    return detected
