# __main__.py

import os, sys, re
import argparse
import pandas as pd
import datetime
import numpy as np
from subprocess import Popen, PIPE

from .algo_calc_flux import *
from .__constants__ import *
from .ecmwf import get_UTC_overpass
from .ecmwf import get_pressure_level_from_alt, get_alt_from_pressure_level

# Get git hash from current executable
def get_git_hash_of_package():
    # Path to executable
    gitdir = os.path.dirname(__file__)
    try:
        process = Popen(["git","-C",gitdir, "rev-parse", "HEAD"], stdout=PIPE)
        (commit_hash, err) = process.communicate()
        exit_code = process.wait()
        return commit_hash.decode("utf-8").strip()
    except:
        return None

# Get computation info from input file header
def read_header(csv_mass):
    # Read file header and retrieve computation info
    search_comput_info = 'Computation info:'
    with open(csv_mass) as f:
        # Loop over lines
        for line in f:
            # Search string
            if re.search(search_comput_info, line):
                # Convert to dict
                comput_info = eval(line.split(search_comput_info)[1])
                # Exit loop
                break
    return comput_info

# Resampling method
def resampler(x, offset_hours="0h"):
    return x.set_index('Time').resample('1D', offset=offset_hours).nearest(limit=1)

# Get cloud fraction from input file
def read_cf_from_csv(csv_cf, header_length=4, utc_time=None):
    # Synchronize data to get one measurement per day
    df_cf_raw = pd.read_csv(csv_cf, header=header_length, index_col=['Time','radius']).sort_index()
    # Specify that time is in UTC (reconstruct index)
    df_cf_raw.index = df_cf_raw.index.set_levels([pd.to_datetime(df_cf_raw.index.levels[0]).tz_localize('UTC'), df_cf_raw.index.levels[1]])
    # Get UTC time of acquisitions
    #if utc_time is None: # use UTC time of first record
    #    utc_time = int(df_mass_raw.index[0][0].hour)
    # Apply half-a-day shift to center time series
    loffset_hours = 6
    offset_hours = '%dH' % (utc_time - loffset_hours)
    # Resample time-series
    df_cf = df_cf_raw.reset_index(level=0).groupby(level=0).apply((lambda x: resampler(x, offset_hours=offset_hours))).swaplevel(0, 1).sort_index(axis=0)
    # Add back loffset
    #df_mass.index.set_levels(df_mass.index.levels[0] + pd.tseries.frequencies.to_offset("%dH" % loffset_hours), level='Time', inplace=True)
    df_cf.index = df_cf.index.set_levels(df_cf.index.levels[0] + pd.tseries.frequencies.to_offset("%dh" % loffset_hours), level='Time')
    return df_cf

# Get integrated masses from input file
def read_mass_from_csv(csv_mass, header_length=4, utc_time=None):
    # Synchronize data to get one measurement per day
    df_mass_raw = pd.read_csv(csv_mass, header=header_length, index_col=['Time','radius']).sort_index()
    # Specify that time is in UTC (reconstruct index)
    df_mass_raw.index = df_mass_raw.index.set_levels([pd.to_datetime(df_mass_raw.index.levels[0]).tz_localize('UTC'), df_mass_raw.index.levels[1]])
    # Get UTC time of acquisitions
    #if utc_time is None: # use UTC time of first record
    #    utc_time = int(df_mass_raw.index[0][0].hour)
    # Apply half-a-day shift to center time series
    loffset_hours = 6
    offset_hours = '%dH' % (utc_time - loffset_hours)
    # Resample time-series
    df_mass = df_mass_raw.reset_index(level=0).groupby(level=0).apply((lambda x: resampler(x, offset_hours=offset_hours))).swaplevel(0, 1).sort_index(axis=0)
    # Add back loffset
    #df_mass.index.set_levels(df_mass.index.levels[0] + pd.tseries.frequencies.to_offset("%dH" % loffset_hours), level='Time', inplace=True)
    df_mass.index = df_mass.index.set_levels(df_mass.index.levels[0] + pd.tseries.frequencies.to_offset("%dh" % loffset_hours), level='Time')
    return df_mass

def export_data_results(comput_info, opts_calc_flux, additional_info, df_results, output_directory, output_file, verbose=False):

    # Output file (.csv format)
    fileout = os.path.join(output_directory, output_file+'.csv')
    if verbose:
        print("Writing result to %s" % fileout)

    with open(fileout, 'w') as f:
        if additional_info['wind_speed_fixed'] is not None:
            f.write('# Estimated flux for %s %s from %s, assuming wind speed of %.1f m/s - %s to %s\n' % (comput_info['variable'].split("_")[0], comput_info['variable'].split("_")[1], comput_info['volcano'], additional_info['wind_speed_fixed'], comput_info['start'], comput_info['end']))
        elif additional_info['pressurelevel_fixed'] is not None:
            f.write('# Estimated flux for %s %s from %s, assuming wind speed from ERA5 at %dhPa (~%.1fkm) - %s to %s\n' % (comput_info['variable'].split("_")[0], comput_info['variable'].split("_")[1], comput_info['volcano'], int(additional_info['pressurelevel_fixed']), get_alt_from_pressure_level(float(additional_info['pressurelevel_fixed'])), comput_info['start'], comput_info['end']))
        elif additional_info['altitude_fixed'] is not None:
            f.write('# Estimated flux for %s %s from %s, assuming wind speed from ERA5 at %.1fkm (%dhPa) - %s to %s\n' % (comput_info['variable'].split("_")[0], comput_info['variable'].split("_")[1], comput_info['volcano'], float(additional_info['altitude_fixed']), int(get_pressure_level_from_alt(float(additional_info['altitude_fixed']))), comput_info['start'], comput_info['end']))
        else:
            f.write('# Estimated flux\n')
        f.write('# Data extracted from the Flux Calculator (command line) - %s\n' % additional_info['UTCtime'].strftime("%Y-%m-%d %H:%M:%S"))
        f.write('# Source: https://git.icare.univ-lille.fr/icare-public/so2-flux-calculator (version %s)\n' % get_git_hash_of_package())
        f.write('# Mass time series info: %s\n' % str(comput_info))
        f.write('# Post-processing filters: %s\n' % str(additional_info))
        f.write('# Flux estimation parameters: %s\n' % str(opts_calc_flux))
    #pd.DataFrame.from_dict(export_dict, orient="index").T.to_csv(fileout, index=False)
    df_results.to_csv(fileout, mode='a') # sent to stdoutput

# Read input file and compute flux
def run(csv_in_mass, output_directory=None, output_file=None, header_length=4, utc_time=None, sigma_CA=0.3, wind_speed_fixed=10., pressurelevel_fixed=None, altitude_fixed=None, confidence_detection=99.0, iter_max=10, method='curvefit', drop_distances = None, pixel_area = 24.5, du_to_kton=0.0007002642684658869, cloud_fraction_file=None, interactive_figure=False, verbose=False):

    ######################################
    ## Read input data files

    # Read header and extract value of CAmin
    comput_info = read_header(csv_in_mass)
    CAmin = float(comput_info['filters']['du'])
    if verbose:
        print("Mass computation info:", comput_info)

    # Determine UTC time from longitude of target
    if utc_time is None: # use UTC time of first record
        # Get UTC time of overpass, and day shift, if needed
        _, utc_time, _ = get_UTC_overpass(comput_info['longitude'])

    # Read integrated masses and synchronize them to UTC time of acquisitions
    df_mass = read_mass_from_csv(csv_in_mass, header_length=header_length, utc_time=utc_time)

    # Read cloud fraction if necessary
    if cloud_fraction_file is not None:
        df_cf = read_cf_from_csv(cloud_fraction_file, header_length=4, utc_time=utc_time)
    else:
        df_cf = None

    ######################################
    ## Drop list of distances, if needed
    if drop_distances is not None:
        #if cloud_fraction_file is not None:
        #    print(set(list_distances) - set(drop_distances))
        #    #df_cf[]
        # Remove distance one at the time
        for drop_distance in drop_distances:
            # Check if distance is in list
            if df_mass.index.levels[1].isin([drop_distance]).any():
                if verbose:
                    print("Dropping distance %d" % drop_distance)
                df_mass = df_mass.drop(index=drop_distance, level=1, axis=0)
            else:
                print("Warning: distance %d km not found in input file. Ignored." % drop_distance)
        # Reset index
        df_mass = df_mass.reset_index().set_index(['Time', 'radius'])

    ######################################
    ## Extract ancillary information at certain radius

    # Get actual list of distances available
    list_distances_valid = list(df_mass.index.levels[1])

    # Get maximum and minimum distance in list (removing dropped distances)
    max_distance = max(list_distances_valid)
    min_distance = min(list_distances_valid)

    # Cloud fraction
    radius_for_cf_default = 250 # default distance for CF: 250 km
    if radius_for_cf_default in list_distances_valid:
        radius_for_cf = radius_for_cf_default
    else: # otherwise use largest distance
        radius_for_cf = max_distance
    if verbose:
        print("Extracting cloud fraction for radius %d km." % radius_for_cf)
    df_cf = df_cf.iloc[df_cf.index.get_level_values(1) == radius_for_cf].droplevel(1) * 100

    # Fraction filtered on edge track at max_distance
    if verbose:
        print("Extracting fraction filtered on swath edge for radius %d km." % max_distance)
    df_fraction_edge = df_mass.iloc[df_mass.index.get_level_values(1) == max_distance].droplevel(1)
    df_fraction_edge = df_fraction_edge[['fraction_filtered_edge_tracks']]

    # Centroid azimuth and distance
    if 'centroid_lon' in df_mass and 'centroid_lat' in df_mass:
        volc_lat, volc_lon = float(comput_info['latitude']), float(comput_info['longitude'])
        radius_for_centroid_default = 100 # default distance for centroid: 100 km
        if radius_for_centroid_default in list_distances_valid:
            radius_for_centroid = radius_for_centroid_default
        else: # otherwise use smallest distance
            radius_for_centroid = min_distance
        if verbose:
            print("Extracting centroid azimuth and distance for radius %d km." % radius_for_cf)
        df_centroid = df_mass.iloc[df_mass.index.get_level_values(1) == radius_for_centroid].droplevel(1)[['centroid_lon', 'centroid_lat']]
        df_centroid['centroid_azimuth'] = 90 - np.rad2deg(np.arctan2((df_centroid['centroid_lat'] - volc_lat)/np.cos(np.deg2rad(volc_lat)), df_centroid['centroid_lon'] - volc_lon))
        df_centroid['centroid_dist_km'] = R_earth/1.e3*np.deg2rad(np.sqrt(((df_centroid['centroid_lat'] - volc_lat)/np.cos(np.deg2rad(volc_lat)))**2 + (df_centroid['centroid_lon'] - volc_lon)**2))
    else:
        df_centroid = None

    ######################################
    ## Set wind speed
    if altitude_fixed is not None: # use ERA-5
        # Get pressure level (nearest from specified atltitude)
        pressurelevel = get_pressure_level_from_alt(altitude_fixed)
    elif pressurelevel_fixed is not None: # use ERA-5
        pressurelevel = pressurelevel_fixed
    else:
        pressurelevel = None
    if pressurelevel is not None: # use ERA-5
        from so2_flux_calculator.ecmwf import run_ECMWF_API_request
        df_wind = run_ECMWF_API_request(comput_info, pressurelevel, output_directory, verbose)
        if verbose:
            print("Wind speed read from input .csv:")
            print(df_wind['wind_speed_magnitude_m_per_s'])
        wind_speed = df_wind['wind_speed_magnitude_m_per_s'].values
        wind_azimuth = df_wind['wind_azimuth_clockwise_from_north_deg'].values
    else:
        wind_speed = wind_speed_fixed

    ###################
    # Estimate lumped parameter and uncertainties
    opts_calc_flux = {'CAmin': CAmin, 'sigma_CA': sigma_CA, 'iter_max': iter_max, 'method': method, 'verbose': verbose, 'pixel_area': pixel_area, 'du_to_kton': du_to_kton}
    result = pd.DataFrame([calc_flux_linquad_weighted(row.droplevel(0), **opts_calc_flux)
                           for _, row in df_mass[comput_info['variable']].groupby(level=0)],
                          index=df_mass.index.levels[0].rename('time'),
                          columns=['a', 'b', 'c', 'sigma_a', 'sigma_b', 'sigma_c', 'sigma_CA_post', 'tstar1_post'])
    result = result.assign(degassing=degassing_detected(result['tstar1_post'], confidence_detection)).drop(columns=['tstar1_post'])

    ######################################
    # Calculate flux using information on wind speed provided by user
    result['flux'] = result['b'] * wind_speed * convert_mps_kmpday # unit : kton/day
    result['sigma_flux'] = result['sigma_b'] * wind_speed * convert_mps_kmpday # unit : kton/day

    ######################################
    ## Add ancillary information for export and further post-processing

    # Wind speed
    if pressurelevel is not None:
        result['speed'] = wind_speed
        result['wind_azimuth'] = wind_azimuth
    else:
        result['speed'] = wind_speed * np.ones(len(result['flux']))

    # Centroid azimuth
    if df_centroid is not None:
        result['centroid_azimuth'] = df_centroid['centroid_azimuth']
        result['centroid_dist_km'] = df_centroid['centroid_dist_km']

    # Cloud fraction
    if cloud_fraction_file is not None:
        result['TROPOMI_CloudFraction_mean'] = df_cf['TROPOMI_CloudFraction_mean'].values

    # Fraction filtered on edge track
    result['fraction_filtered_edge_tracks'] = df_fraction_edge

    ######################################
    ## Export, save, visualize results

    # Save additional information
    dateutcnow = datetime.datetime.now(datetime.timezone.utc)
    additional_info = {'UTCtime': dateutcnow,
        'mean_wind_speed': np.nanmean(wind_speed),
        'wind_speed_fixed': wind_speed_fixed,
        'pressurelevel_fixed': pressurelevel_fixed,
        'altitude_fixed': altitude_fixed,
        'radius_for_cf' : radius_for_cf,
        'included_radii': list_distances_valid}
    if radius_for_centroid is not None:
        additional_info['radius_for_centroid'] = radius_for_centroid
    export_dict = {**comput_info, **opts_calc_flux, **additional_info}

    # Exports
    if output_file is not None: # write result to disk and figure
        # .csv export
        export_data_results(comput_info, opts_calc_flux, additional_info, result, output_directory, output_file, verbose)
        # .png export
        from so2_flux_calculator.plot import plot_data_results
        plot_data_results(df_mass, result, output_directory, output_file, dateutcnow, comput_info, verbose)
    else: # sent result to stdoutput
        print(comput_info)
        print(opts_calc_flux)
        print(result)

    # Interactive figure
    if interactive_figure:
        from so2_flux_calculator.plot import plot_data_results_interactive
        plot_data_results_interactive(df_mass, result, output_directory, output_file, dateutcnow, comput_info, export_dict)


def parse_args():
    parser=argparse.ArgumentParser(
        add_help=False,
        prog="so2_flux_calculator",
        description="Compute SO2 flux from integrated masses around volcano. Please use input file provided by the VolcPlume portal. Developed primarily for TROPOMI.")
    parser.add_argument('input_file', help="Specify the input file (.csv format)", type=valid_file)
    parser.add_argument('cloud_fraction_file', help="Cloud fraction input file (.csv format)", type=valid_file) # -c

    # parser.add_argument('--wind_speed', help="Wind speed (m/s)", default=10, type=float) # -w
    # parser.add_argument('--pressurelevel', help="Pressure level (hPa)", choices=list_pressurelevels, type=int) # -p

    # Velocity
    group = parser.add_mutually_exclusive_group(required=True)
    # Case 1 : constant velocity
    group.add_argument('--wind_speed', help="Wind speed (m/s)", type=float) # -w
    # Case 2 : get velocity from ECMWF pressure level
    group.add_argument('--pressurelevel', help="Pressure level (hPa)", choices=list_pressurelevels, type=int) # -p
    # Case 3 : get velocity from ECMWF pressure level interpolated at specified altitude
    group.add_argument('--altitude', help="Altitude (km)", type=float) # -a

    parser.add_argument('--confidence_detection', help="Percentage of confidence of statistical test for detection", default=99.0, choices=Range(50.,100.), type=float)
    parser.add_argument('--method', help="Inversion method", default='curvefit', choices=['curvefit', 'polyfit', 'algebraic', 'algebraic2']) # -m
    parser.add_argument('--drop_distances', help="List of distances to ignore in inversion", default=None, choices=list_distances, nargs='*', type=int) # -d
    parser.add_argument('--utc_time', help="UTC time of acquisitions", type=int) # -u
    parser.add_argument('--interactive_figure', help="Interactive plot", default=False, action='store_true') # -f
    parser.add_argument('--output_file', help="Output file name (without extension). Will export results to a .csv file and plot results in a .png file. Files are written to output directory.", default='so2_flux_calculator_out') # -o
    parser.add_argument('--output_directory', help="Output directory", default=os.getcwd()) # -o
    #parser.add_argument('--test', help="Test", default=False, action='store_true') # -t
    parser.add_argument('--verbose', help="Verbose", default=False, action='store_true') # -v
    parser.add_argument('--version', help="Version of code (git hash of executable)", action='version', version='%s' % get_git_hash_of_package())
    parser.add_argument('-h', '--help', action='help', default=argparse.SUPPRESS,
                    help='Show this help message and exit.')
    args=parser.parse_args()
    return args

def valid_file(param):
    base, ext = os.path.splitext(param)
    if ext.lower() not in ['.csv']:
        raise argparse.ArgumentTypeError('File must have a csv or tab extension')
    return param

# Argument as float with boundaries
# From https://stackoverflow.com/a/59678681
class Range(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
    def __eq__(self, other):
        return self.start <= other <= self.end
    def __contains__(self, item):
        return self.__eq__(item)
    def __iter__(self):
        yield self
    def __repr__(self):
        return '[{0},{1}]'.format(self.start, self.end)

def main():

    # Parse arguments
    inputs=parse_args()
    input_file = inputs.input_file
    output_file = inputs.output_file
    wind_speed = inputs.wind_speed
    if wind_speed is None:
        altitude = inputs.altitude
    else:
        altitude = None
    if wind_speed is None and altitude is None:
        pressurelevel = inputs.pressurelevel
    else:
        pressurelevel = None
    utc_time = inputs.utc_time
    cloud_fraction_file = inputs.cloud_fraction_file
    if utc_time is not None and utc_time < 0:
        utc_time = None
        print("Warning: argument utc_time must be positive. Using default instead.")
    confidence_detection = inputs.confidence_detection
    method = inputs.method
    drop_distances = inputs.drop_distances
    output_directory = inputs.output_directory
    interactive_figure = inputs.interactive_figure
    verbose = inputs.verbose

    ## Sanity checks

    # Set output directory
    if output_file is not None:
        if output_directory is None:
            print("Please provide output directory with command line option \"--output_directory\".")
            sys.exit(1)
        else:
            if not(os.path.isdir(output_directory)):
                print("Creating non-existing output directory %s." % output_directory)
                try:
                    os.makedirs(output_directory)
                except:
                    print("Could not make new directory %s. Please check permissions." % output_directory)
                    sys.exit(1)

    # Feed arguments into the function that runs the inversion
    opts_run = {'output_directory': output_directory,
        'output_file': output_file,
        'sigma_CA': TROPOMI_sigma_CA,
        'wind_speed_fixed': wind_speed,
        'pressurelevel_fixed': pressurelevel,
        'altitude_fixed': altitude,
        'utc_time': utc_time,
        'confidence_detection': confidence_detection,
        'iter_max': iter_max,
        'method': method,
        'drop_distances': drop_distances,
        'pixel_area': TROPOMI_pixel_area_m2/1e6,
        'du_to_kton': TROPOMI_du_to_kton,
        'cloud_fraction_file': cloud_fraction_file,
        'interactive_figure': interactive_figure,
        'header_length': header_length,
        'verbose': verbose}

    run(input_file, **opts_run)
