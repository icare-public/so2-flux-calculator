# # Parameters with global scope

# # # # # # # # # # # # # # # # # # # # #
#
# Controls for algorithm
#

header_length=4
iter_max = 10
list_distances = [25,50,75,100,125,150,175,200,250,300,350,400,500,1000]
list_pressurelevels = [1, 2, 3,
            5, 7, 10, 20, 30, 50, 70, 100, 125, 150, 175,
            200, 225, 250, 300, 350, 400, 450,
            500, 550, 600, 650, 700, 750, 775, 800, 825,
            850, 875, 900, 925, 950, 975, 1000
        ]

# # # # # # # # # # # # # # # # # # # # #
#
# General constants for SO2 and DU
#
# Unit conversion factors
convert_mps_kmpday=86.400 # multiply by convert_mps_kmpday to convert speed in m/s to km/day
convert_grams_kton = 1e-9 # multiply by convert_grams_kton to convert mass in grams to kton
convert_m2_cm2 = 1e4      # multiplt by convert_m2_cm2     to convert area in m2 to cm2

# Constants
molar_mass_SO2 = 64.066 # molar mass of SO2 (g.mol-1)
avogadro_number=6.02214076e23 # Avogadro number (mol-1)
du_in_molecules_per_cm2=2.6867e16 # 1 DU = 2.6867 x 10**16 molecules/cm2

# Factors for converting DU to SI or other useful units
du_in_molecules_per_m2=du_in_molecules_per_cm2*convert_m2_cm2  # molecules/m2
du_in_mol_per_m2=du_in_molecules_per_m2/avogadro_number  # mol/m2
du_in_grams_per_m2 = du_in_mol_per_m2*molar_mass_SO2  # g/m2
du_in_ktons_per_m2 = du_in_grams_per_m2*convert_grams_kton  # kton/m2
factor_DU_to_mol_per_m2 = avogadro_number/(du_in_molecules_per_cm2*10000) # mol-1/molecules*m2
#print("factor_DU_to_mol_per_m2", factor_DU_to_mol_per_m2)

g0 = 9.80665 # standard gravity (m/s2)
R_earth = 6371e3 # average Earth radius (m)

# # # # # # # # # # # # # # # # # # # # #
#
# Constants specific to TROPOMI
#
# A priori uncertainty for TROPOMI (unit: DU)
TROPOMI_sigma_CA = 0.3 # 1-sigma uncertainty on pixel
# Pixel area for TROPOMI (unit: m2)
TROPOMI_pixel_area_m2 = 3.5*7*1000*1000
# Factor for converting DU to kton, given TROPOMI pixel size (warning: depends on pixel size)
# multiply by TROPOMI_du_to_kton to convert pixel value in DU to kton
TROPOMI_du_to_kton = TROPOMI_pixel_area_m2*convert_grams_kton*molar_mass_SO2/factor_DU_to_mol_per_m2
#print("TROPOMI_du_to_kton", TROPOMI_du_to_kton)
# local time of overpass : 13:30
TROPOMI_local_time = 13.5
