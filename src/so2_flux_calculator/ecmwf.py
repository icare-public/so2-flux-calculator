import os, sys, re
import numpy as np
import pandas as pd

from .__constants__ import *

# # # # # # # # # # # # # # # # # # # # # # # #
#
# Functions borrowed from pvlib (https://pvlib-python.readthedocs.io/en/stable/_modules/pvlib/atmosphere.html)
#
# # #

def pres2alt(pressure):
    '''
    Determine altitude from site pressure.

    Parameters
    ----------
    pressure : numeric
        Atmospheric pressure. [Pa]

    Returns
    -------
    altitude : numeric
        Altitude above sea level. [m]

    Notes
    ------
    The following assumptions are made

    ============================   ================
    Parameter                      Value
    ============================   ================
    Base pressure                  101325 Pa
    Temperature at zero altitude   288.15 K
    Gravitational acceleration     9.80665 m/s^2
    Lapse rate                     -6.5E-3 K/m
    Gas constant for air           287.053 J/(kg K)
    Relative Humidity              0%
    ============================   ================

    References
    -----------
    .. [1] "A Quick Derivation relating altitude to air pressure" from
       Portland State Aerospace Society, Version 1.03, 12/22/2004.
    '''

    alt = 44331.5 - 4946.62 * pressure ** (0.190263)

    return alt

def alt2pres(altitude):
    '''
    Determine site pressure from altitude.

    Parameters
    ----------
    altitude : numeric
        Altitude above sea level. [m]

    Returns
    -------
    pressure : numeric
        Atmospheric pressure. [Pa]

    Notes
    ------
    The following assumptions are made

    ============================   ================
    Parameter                      Value
    ============================   ================
    Base pressure                  101325 Pa
    Temperature at zero altitude   288.15 K
    Gravitational acceleration     9.80665 m/s^2
    Lapse rate                     -6.5E-3 K/m
    Gas constant for air           287.053 J/(kg K)
    Relative Humidity              0%
    ============================   ================

    References
    -----------
    .. [1] "A Quick Derivation relating altitude to air pressure" from
       Portland State Aerospace Society, Version 1.03, 12/22/2004.
    '''

    press = 100 * ((44331.514 - altitude) / 11880.516) ** (1 / 0.1902632)

    return press

# # # # # # # # # # # # # # # # # # # # # # # #

# Return pressure level for a given altitude
# Input: altitude (km)
# Output: pressure (hPa)
def get_pressure_level_from_alt(altitude):
    pressure = alt2pres(altitude*1000.)/100.
    if pressure > list_pressurelevels[-1]:
        return list_pressurelevels[-1]
    elif pressure < list_pressurelevels[0]:
        return list_pressurelevels[0]
    else:
        return list_pressurelevels[np.digitize(pressure,list_pressurelevels, right=True)]

# Return altitude for a given pressure level
# Input: pressure (hPa)
# Output: altitude (km)
def get_alt_from_pressure_level(pressure):
    altitude = pres2alt(pressure*100.)/1000.
    return altitude

# Compute height above sea level from a given geopotential.
def geopotential_to_height(geopt, g0 = 9.80665, R_earth = 6371e3):
    #g0 = 9.80665 # standard gravity (m/s2)
    #R_earth = 6371e3 # average Earth radius (m)
    return (geopt * R_earth) / (g0 * R_earth - geopt)

# A (very) rough estimation of UTC time of overpass by TROPOMI
def get_UTC_overpass(longitude):
    local_time = ((((-longitude+180)/360. % 360))*24.) + TROPOMI_local_time - 12.
    local_time_wrapped = int(np.round(local_time % 24))
    day_shift = int((local_time_wrapped - local_time)/24)
    return local_time, local_time_wrapped, day_shift

def read_wind_speed_from_csv(fileout_wind, verbose=False):
    # Synchronize data to get one measurement per day
    df_wind_raw = pd.read_csv(fileout_wind+".csv", index_col='Date', header=1)
    return df_wind_raw

def run_ECMWF_API_request(comput_info, pressurelevel, output_directory, verbose=False):
#def set_ECMWF_API_request(comput_info):

    fileout_wind = os.path.join(output_directory, "ERA5_wind_%s_%04dhPa_%s_%s" % (comput_info['volcano'].replace(' ', '-').replace(',', ''), pressurelevel, comput_info['start'], comput_info['end']))

    if os.path.isfile("%s.csv" % fileout_wind):
        print("Warning: File already exists. Will attempt reading from this file. Please delete file or rename it if you expect extraction from NetCDF to be done.")

    else:
        if verbose:
            print("NetCDF output file for ECMWF API request: %s.nc" % fileout_wind)
        if os.path.isfile("%s.nc" % fileout_wind):
            print("Warning: File already exists. Will attempt reading from this file. Please delete file or rename it if you expect API request to be executed.")
        else:
            if verbose:
                print("Requesting wind speed from ECMWF's API.")
            trigger_ECMWF_API_request(fileout_wind, comput_info, pressurelevel, verbose)

        if verbose:
            print("Reading wind field data in NetCDF file returned by ECMWF's API request in %s, and apply conversion to .csv." % (fileout_wind+'.nc'))
        read_ECMWF_API_request(fileout_wind, verbose)

    if verbose:
        print("Reading wind field data in %s." % (fileout_wind+'.csv'))
    df_wind = read_wind_speed_from_csv(fileout_wind, verbose)

    return df_wind

def trigger_ECMWF_API_request(filout, comput_info, pressurelevel, verbose=False):

    # Space-time extent of request
    request_latitude = comput_info['latitude']
    request_longitude = comput_info['longitude']
    request_datestart = comput_info['start']
    request_dateend = comput_info['end']

    # Get UTC time of overpass, and day shift, if needed
    _, request_local_time_wrapped, request_day_shift = get_UTC_overpass(request_longitude)

    # Set request area
    request_area = [request_latitude, request_longitude, request_latitude, request_longitude]

    # Apply 1-day offset if necessary (around +/-180°, in the Pacific)
    if request_day_shift != 0:
        request_datestart = pd.to_datetime(request_datestart).tz_localize('UTC') - pd.DateOffset(days=request_day_shift)
        request_dateend = pd.to_datetime(request_dateend).tz_localize('UTC') - pd.DateOffset(days=request_day_shift)

    # Set pressure level
    request_pressurelevel = '%d' % pressurelevel

    # Fill in query parameters
    opts_request_default = {"format": "netcdf",
      "product_type": "reanalysis",
      "variable": ['geopotential',"u_component_of_wind", "v_component_of_wind"]
        }
    opts_request = {"area": request_area,
        "date": [request_datestart+"/"+request_dateend],
        "pressure_level": [request_pressurelevel],
        "time": "%02d:00" % request_local_time_wrapped,
        }

    # Run the query
    import cdsapi
    capi = cdsapi.Client()
    if verbose:
        print("ECMWF API request parameters:")
        print({**opts_request, **opts_request_default})
    print("ECMWF API running, please wait...")
    capi.retrieve("reanalysis-era5-pressure-levels", {**opts_request, **opts_request_default}, filout+'.nc')
    print("Done!")


def read_ECMWF_API_request(fileout_wind, verbose=False):

    # Read NetCDF file. Assumes that there is only one point in spatial coordinates.
    import xarray as xr
    ds_windfield = xr.open_dataset(fileout_wind + '.nc', decode_cf=True)

    if 'expver' in ds_windfield.coords:
        if verbose:
            print("Warning: merging ERA5 (expver=1) and ERA5T (expver=5) in %s." % fileout_wind)
        ds_windfield = xr.merge([ds_windfield.sel(expver=1).drop('expver', dim=None), ds_windfield.sel(expver=5).drop('expver', dim=None)], compat="no_conflicts")

    wind_u = ds_windfield['u'].values.reshape(-1) # eastward component of wind (m/s)
    wind_v = ds_windfield['v'].values.reshape(-1) # northward component of wind (m/s)
    wind_geopt = ds_windfield['z'].values.reshape(-1) # geopotential
    wind_time = ds_windfield['time'].values.reshape(-1) # time

    # Deduce wind speed and direction
    wind_speed = np.sqrt(wind_u**2 + wind_v**2)
    wind_azimuth = np.rad2deg(np.arctan2(wind_u, wind_v))

    # Altitude of pressure level
    wind_altitude = np.nanmean(geopotential_to_height(wind_geopt))

    # Export
    df_wind_from_netcdf = pd.DataFrame(np.vstack([wind_speed.T, wind_u.T, wind_v.T, wind_azimuth.T]).T, columns=['wind_speed_magnitude_m_per_s','wind_speed_east_m_per_s','wind_speed_north_m_per_s','wind_azimuth_clockwise_from_north_deg'], index=wind_time)
    df_wind_from_netcdf.index.names = ['Date']
    with open(fileout_wind + ".csv", 'w') as file:
        file.write('# Wind altitude: %f m\n' % wind_altitude)
        df_wind_from_netcdf.to_csv(file, mode='a')

    return df_wind_from_netcdf
