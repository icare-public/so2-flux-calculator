
import scipy.special as sp
import numpy as np

# Curve fitting functions (for curve_fit method)
def fit_func_lin_zerointercept(x, a):
    return a * x  # 0 at x=0 is implied => Fits y = a*r
def fit_func_quad_zerointercept(x, b):
    return b * x**2  # 0 at x=0 is implied => Fits y = b*r^2
def fit_func_lin_quad_zerointercept(x, a, b):
    return a * x + b * x**2  # 0 at x=0 is implied => Fits y = a*r + b*r^2
def fit_func_lin_quad_withintercept(x, a, b, c):
    return a + b * x + c * x**2  # 0 at x=0 is implied => Fits y = a + b*r + c*r^2

# Elementary functions to derive expectation and std of truncated-normal distribution
def fphi(alpha): # pdf of standard normal distribution
    return 1/np.sqrt(2*np.pi)*np.exp(-1/2*alpha**2)
def fF(alpha): # cdf of standard normal distribution
    return sp.ndtr(alpha)
def fZ(alpha):
    return fphi(alpha)/(1-fF(alpha))
def fdelta(alpha):
    return fZ(alpha)*(fZ(alpha)-alpha)

# Functions related to Lambert W function (sp.lambertw)
def func_fw(x, a): # Forward function
    return np.abs(x*np.exp(-(a/x)**2))
def inv_func_fw(a, b): # Converse relation of func_fw
    return(np.sqrt((2*a**2)/(np.abs(sp.lambertw(2*a**2/b**2)))))
def inv_func_fw_scaled(_CAmin, _target_y, _pixel_area=24.500000, _du_to_kton=0.0007002642684658869): # Scaled version of converse function
    a = _CAmin/np.sqrt(2)
    b = _target_y / (np.sqrt(np.pi/2)*_du_to_kton/_pixel_area)
    if a == 0: # if a=0, func_fw(x,0) = x
        return b
    else:
        return inv_func_fw(a, b)
