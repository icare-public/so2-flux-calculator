
import subprocess

import pandas as pd
import pytest


@pytest.mark.parametrize('input_files, args, expected', [
    (['tests/inputs/input_Etna_mass.csv', 'tests/inputs/input_Etna_cloud.csv'],
     [], 'output_Etna'),
    (['tests/inputs/input_Piton_mass.csv', 'tests/inputs/input_Piton_cloud.csv'],
     ['--drop_distances', '1000', '--wind_speed', '6'], 'output_Piton'),
])
def test_acceptance_1(input_files, args, expected):
    subprocess.run(['so2_flux_calculator']
                   + input_files
                   + args
                   + ['--output_file', expected, '--output_directory', './tests/actual'])

    actual = pd.read_csv(f'./tests/actual/{expected}.csv', skiprows=2)
    expected = pd.read_csv(f'./tests/expected/{expected}.csv', skiprows=2)
    pd.testing.assert_frame_equal(actual, expected, check_exact=False)
