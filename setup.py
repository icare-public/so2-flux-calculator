#!/usr/bin/env python

import setuptools

if __name__ == "__main__":
    setuptools.setup()

# from setuptools import setup
#
# with open("requirements.txt") as f:
#     requirements = [line.strip() for line in f.readlines()]
#
# setup(name='so2_flux_calculator',
#       version='0.2.1',
#       description='SO2 flux calculator',
#       url='http://github.com/RaphaelGrandin/volcplume_flux_calculator',
#       author='Raphael Grandin',
#       author_email='grandin@ipgp.fr',
#       license='MIT',
#       packages=['so2_flux_calculator'],
#       package_dir={"so2_flux_calculator": "src/so2_flux_calculator"},
#       install_requires=requirements,
#       entry_points={'console_scripts': ['so2_flux_calculator=so2_flux_calculator.__main__:main']},
#       zip_safe=False)
